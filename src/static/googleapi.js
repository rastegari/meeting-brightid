var js = document.createElement("script");
js.async = true;
js.defer = true;
js.src = 'https://apis.google.com/js/api.js'
document.body.appendChild(js);
// Client ID and API key from the Developer Console
var CLIENT_ID = '100227490864-hq5ooidf3jmlp168fd2v7lfeik1s3fe9.apps.googleusercontent.com';
var API_KEY = 'AIzaSyC_IlCvbBpH_pqf9ejw454_p0yrKKtL4d4';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = "https://www.googleapis.com/auth/calendar.events";

var start;
var end;
var summary;
var location_temp;
/**
*  On load, called to load the auth2 library and API client library.
*/
function handleClientLoad(summary_time ,start_time , end_time, location_time) {
    start = start_time;
    summary = summary_time;
    end = end_time;
    location_temp = location_time;
    gapi.load('client:auth2', initClient);
}

/**
*  Initializes the API client library and sets up sign-in state
*  listeners.
*/
function initClient() {
    gapi.client.init({
    apiKey: API_KEY,
    clientId: CLIENT_ID,
    discoveryDocs: DISCOVERY_DOCS,
    scope: SCOPES
    }).then(function () {
        // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

        // Handle the initial sign-in state.
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    }, function(error) {
        appendPre(JSON.stringify(error, null, 2));
    });
}

/**
*  Called when the signed in status changes, to update the UI
*  appropriately. After a sign-in, the API is called.
*/
function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
    //   listUpcomingEvents();
        createEvent(summary, start, end, location)
    }else{
        // signin if user click this for first time
        gapi.auth2.getAuthInstance().signIn();
    }
}

function createEvent(summary, start, end, location){
    var event = {
        'summary': summary,
        'location': location_temp,
        'description': null,
        'start': start,
        'end': end,
        'recurrence': null,
        'attendees': null,
        'reminders': null
    };
    var request = gapi.client.calendar.events.insert({
        'calendarId': 'primary',
        'resource': event
    });

    request.execute(function(event) {
            appendPre('Event created: ' + event.htmlLink);
    });
}

/**
* Append a pre element to the body containing the given message
* as its text node. Used to display the results of the API call.
*
* @param {string} message Text to be placed in pre element.
*/
function appendPre(message) {
    var pre = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
}